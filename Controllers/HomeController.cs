﻿using HackatonWebApp.Backend;
using HackatonWebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HackatonWebApp.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            List<string> townNames = new List<string>();
            List<short> riskFactor = new List<short>();
            List<short> windFactor = new List<short>();
            List<short> rainFactor = new List<short>();

            jsonParser parser = new jsonParser();
            List<gridInfo> receiveData = parser.reveiveData(new DateTime(2012, 10, 19, 00, 00, 00));

            csvParser csvData = new csvParser();
            List<townInformation> townInfo =  csvData.parseCSV();

            foreach (gridInfo gridIterator in receiveData)
            {
                foreach(townInformation townIterator in townInfo)
                {
                    townNames.Add(townIterator.townName);

                    int coordinateCounter = 0;
                    foreach (float xCoordinate in townIterator.x)
                    {
                        if (xCoordinate > gridIterator.x && xCoordinate < gridIterator.x + 0.25)
                        {
                            if (townIterator.y[coordinateCounter] > gridIterator.y && townIterator.y[coordinateCounter] < gridIterator.y + 0.25)
                            {
                                float rainFactorFL = (float)    ((15.0 / gridIterator.Precipitation[0]) * 100.0);
                                if (rainFactorFL > 100) rainFactorFL = (float) 100.0;
                                float windFactorFL = (float)    ((70.0 / gridIterator.wind[0]) * 100.0);
                                if (windFactorFL > 100) windFactorFL = (float) 100.0;

                                rainFactor.Add((short)  rainFactorFL);
                                windFactor.Add((short)  windFactorFL);

                                float riskFactorFL = (float) 0.0;
                                if (rainFactorFL > 75 && windFactorFL > 75)
                                {
                                    riskFactorFL = (townIterator.population / 9000000) * 100;
                                    if (riskFactorFL > 100) riskFactorFL = (float) 100.0;
                                    riskFactor.Add((short) ((riskFactorFL + windFactorFL + rainFactorFL) / 100));
                                } 
                                //riskFactor.Add((short) ((15.0 / gridIterator.Precipitation[0]) * 100.0));
                            }
                        }
                    }
                    coordinateCounter++;
                }
            }
            ViewData["townNames"] = townNames;
            ViewData["riskFactor"] = riskFactor;
            ViewData["windFactor"] = windFactor;
            ViewData["rainFactor"] = rainFactor;

            return View("Home");
        }
    }
}