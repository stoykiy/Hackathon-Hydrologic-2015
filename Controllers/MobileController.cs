﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HackatonWebApp.Controllers
{
    public class MobileController : Controller
    {
        // GET: Mobile
        public ActionResult Index()
        {
            return View("Mobile");
        }

        //Get: Information
        public ActionResult Information()
        {
            return View("Information");
        }
    }
}