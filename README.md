# Hackathon Hydrologic 2015

In the weekend of 12 and 13 december 2015 I attended a hackathon. 
The task was given to create an application that was able to 
warn civilians and the government for hurricanes. In 28 hours we created
this application to show the trail of hurricane Sandy. The application is 
built with a Bootstrap front-end, and C# / ASP.net MVC5 backend.

Impression of the GUI showing the trail of hurricane Sandy. The colored circles
are geofences with different regions. If civilians fall within certain geofences,
their evacuation priority changes accordingly. At the bottom, a slider is shown
where we can move through time to show how the hurricane moves.
![alt tag](https://raw.githubusercontent.com/remyjaspers/Hackathon-Hydrologic-2015/master/Images/sandy_trail.bmp)

Another feature this application contains is the ability to plan routes to evacuation shelters
automatically when a civilian is too close to the hurricane.
![alt tag](https://raw.githubusercontent.com/remyjaspers/Hackathon-Hydrologic-2015/master/Images/evac_routes.bmp)