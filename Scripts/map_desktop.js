﻿/*
    Person 1
    Start and end points for a route to evac
    Coordinate of current person
*/
var start1 = "Mayfair Drive North, Brooklyn, New York City, New York, Verenigde StatenAvenue M";
var end1 = "901 CLASSON AVENUE";
var person1_coords;

/*
    Person 2
    Start and end points for a route to evac
    Coordinate of current person
*/
var start2 = "Flatlands 6th Street, Brooklyn, New York, Verenigde Staten";
var end2 = "1100 E New York Ave, Brooklyn, NY 11212, Verenigde Staten";
var person2_coords;

/*
    Person 3
    Start and end points for a route to evac
    Coordinate of current person
*/
var start3 = "Compass Road, Jamaica, New York, Verenigde Staten";
var end3 = "87-41 Parsons Blvd,Queens, NY 11432,Verenigde Staten";
var person3_coords;


var map, marker, sandy_trail;

//Create three variables to hold geofences of the hurricane
var hurricane_red_zone;
var hurricane_yellow_zone;
var hurricane_green_zone;



function initMap() {

    map = new google.maps.Map(document.getElementById('map-canvas'), {
        center: { lat: 40.730610, lng: -73.935242 },
        zoom: 4
    });

//Init sandy trail for use in the timeline
    sandy_trail = [
                    new google.maps.LatLng(14.30, -77.40),
                    new google.maps.LatLng(13.9, -77.8),
                    new google.maps.LatLng(13.5, -78.2),
                    new google.maps.LatLng(13.1, -78.6),
                    new google.maps.LatLng(12.7, -78.7),
                    new google.maps.LatLng(12.6, -78.4),
                    new google.maps.LatLng(12.9, -78.1),
                    new google.maps.LatLng(13.4, -77.9),
                    new google.maps.LatLng(14, -77.6),
                    new google.maps.LatLng(14.7, -77.3),
                    new google.maps.LatLng(16.6, -76.9),
                    new google.maps.LatLng(17.7, -76.7),
                    new google.maps.LatLng(17.9, -76.6),
                    new google.maps.LatLng(18.9, -76.4),
                    new google.maps.LatLng(20, -76),
                    new google.maps.LatLng(20.1, -76),
                    new google.maps.LatLng(20.9, -75.7),
                    new google.maps.LatLng(21.7, -75.5),
                    new google.maps.LatLng(23.3, -75.3),
                    new google.maps.LatLng(24.8, -75.9),
                    new google.maps.LatLng(25.7, -76.4),
                    new google.maps.LatLng(26.4, -76.9),
                    new google.maps.LatLng(27, -77.2),
                    new google.maps.LatLng(27.5, -77.1),
                    new google.maps.LatLng(28.1, -76.9),
                    new google.maps.LatLng(28.8, -76.5),
                    new google.maps.LatLng(29.7, -75.6),
                    new google.maps.LatLng(30.5, -74.7),
                    new google.maps.LatLng(31.3, -73.9),
                    new google.maps.LatLng(32, -73),
                    new google.maps.LatLng(32.8, -72),
                    new google.maps.LatLng(33.9, -71),
                    new google.maps.LatLng(35.3, -70.5),
                    new google.maps.LatLng(36.9, -71),
                    new google.maps.LatLng(38.3, -73.2),
                    new google.maps.LatLng(38.8, -74),
                    new google.maps.LatLng(39.4, -74.4),
                    new google.maps.LatLng(39.5, -74.5),
                    new google.maps.LatLng(39.9, -76.2),
                    new google.maps.LatLng(40.1, -77.8),
                    new google.maps.LatLng(40.4, -78.9),
                    new google.maps.LatLng(40.7, -79.8),
                    new google.maps.LatLng(41.1, -80.3),
                    new google.maps.LatLng(41.5, -80.7)
    ];
    
    //Set person coords
    person1_coords = new google.maps.LatLng(40.6192012, -73.9659634);
    person2_coords = new google.maps.LatLng(40.6429129, -73.8880607);
    person3_coords = new google.maps.LatLng(40.6549908, -73.7950092);

    //Initiate direction service for plotting the routes to evac
    var directionsService = new google.maps.DirectionsService;
    var directionsDisplay1 = new google.maps.DirectionsRenderer;
    var directionsDisplay2 = new google.maps.DirectionsRenderer;
    var directionsDisplay3 = new google.maps.DirectionsRenderer;

    directionsDisplay1.setMap(map);
    directionsDisplay2.setMap(map);
    directionsDisplay3.setMap(map);

    calculateAndDisplayRoute(directionsService, directionsDisplay1, start1, end1);
    calculateAndDisplayRoute(directionsService, directionsDisplay2, start2, end2);
    calculateAndDisplayRoute(directionsService, directionsDisplay3, start3, end3);

    //Initiate hurricane geofences
    hurricane_redzone = new google.maps.Circle({
        center: { lat:13.1,lng:-78.6 },
        radius: 200000,
        strokeColor: "#e74c3c",
        strokeOpacity: 1,
        strokeWeight: 2,
        fillColor: "#e74c3c",
        fillOpacity: 0.7,
        zIndex: 0
    });
    hurricane_redzone.setMap(map);

    hurricane_yellow = new google.maps.Circle({
        center: { lat: 13.1, lng: -78.6 },
        radius: 300000,
        strokeColor: "#e67e22",
        strokeOpacity: 1,
        strokeWeight: 2,
        fillColor: "#e67e22",
        fillOpacity: 0.5,
        zIndex: 1
    });
    hurricane_yellow.setMap(map)

    hurricane_green = new google.maps.Circle({
        center: { lat: 13.1, lng: -78.6 },
        radius: 400000,
        strokeColor: "#2ecc71",
        strokeOpacity: 1,
        strokeWeight: 2,
        fillColor: "#2ecc71",
        fillOpacity: 0.2,
        zIndex: 2
    });
    hurricane_green.setMap(map);

    var evac = new google.maps.FusionTablesLayer({
        query: {
            select: 'Geocodable address',
            from: '18v2n3Gc4Wb2sSse5oqb7Q3vu_bKF1UN9qksmCKo'
        },
        options: {
            styleId: 2,
            templateId: 2
        }
    });

    var ny_towns = new google.maps.FusionTablesLayer({
        query: {
            select: 'geometry',
            from: '14HylIzFNBD7lPa_BkaugBvBPoI_DkOQukNxDvqZ3'
        },
        styles: [
            {
                polygonOptions: {
                    fillColor: "00ff00",
                    fillOpacity: 0.2,
                    strokeOpacity: 0.0,
                    strokeColor: "FFFFFF"
                }
            },
			
   
        ],
    });

    // Plot the points of the storm for the timeslider
    // Obtained from: http://weather.unisys.com/hurricane/atlantic/2012/SANDY/track.dat
    var sandy_path = new google.maps.FusionTablesLayer({
        query: {
            select: 'Geocodable address',
            from: '1sVO24p2dfO7jhrsjZfkn8_pBz32nyEygqdOiBs8'
        },
        options: {
            styleId: 2,
            templateId: 2
        }
    });

   
    sandy_path.setMap(map);
    ny_towns.setMap(map);
    evac.setMap(map);
}

//Function to calculate and display route, obtained from google dev site
function calculateAndDisplayRoute(directionsService, directionsDisplay, start, end) {
    directionsService.route({
        origin: start,
        destination: end,
        travelMode: google.maps.TravelMode.DRIVING
    }, function (response, status) {
        if (status === google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
        } else {
            window.alert('Directions request failed due to ' + status);
        }
    });
}

// Initiate slider for timeline
$('#flat-slider').slider({
    orientation: 'horizontal',
    range: false,
    value: 0,
    min: 3,
    max: 43
});


// Do stuff on slider change
$("#flat-slider").slider({
    slide: function (event, ui) {
        
        // Move circles with the hurricane
        hurricane_redzone.setCenter(sandy_trail[ui.value]);
        hurricane_yellow.setCenter(sandy_trail[ui.value]);
        hurricane_green.setCenter(sandy_trail[ui.value]);
      
        // Calculate distance between center of circle 
        // and person to change their evac priority
        var distance1 = google.maps.geometry.spherical.computeDistanceBetween(sandy_trail[ui.value], person1_coords);
        var distance2 = google.maps.geometry.spherical.computeDistanceBetween(sandy_trail[ui.value], person2_coords);
        var distance3 = google.maps.geometry.spherical.computeDistanceBetween(sandy_trail[ui.value], person3_coords);

        // Change data table according to distances
        // Most ugly if clause I've ever written, have mercy oh good lord Gaben
        var oTable = $('#example').dataTable();
        oTable.fnUpdate('Low', 0, 4);
        oTable.fnUpdate('Low', 1, 4);
        oTable.fnUpdate('Low', 2, 4);
        if (distance1 < 400000 || distance2 < 400000 || distance3 < 400000) {
            oTable.fnUpdate('Low', 0, 4);
            oTable.fnUpdate('Low', 1, 4);
            oTable.fnUpdate('Low', 2, 4);
        } 
        if (distance1 < 300000 || distance2 < 300000 || distance3 < 300000) {
            oTable.fnUpdate('Medium', 0, 4);
            oTable.fnUpdate('Medium', 1, 4);
            oTable.fnUpdate('Medium', 2, 4);
        }
        if (distance1 < 200000 || distance2 < 200000 || distance3 < 200000) {
            oTable.fnUpdate('High', 0, 4);
            oTable.fnUpdate('High', 1, 4);
            oTable.fnUpdate('High', 2, 4);
        }
    }
});


// Initiate the DataTable
$(document).ready(function () {
    $('#example').DataTable();
    
      
    
});
