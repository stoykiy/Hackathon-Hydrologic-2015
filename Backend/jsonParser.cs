﻿//using HackatonWebApp.Models;
using HackatonWebApp.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;

/*
 *Funtion to parse information from te file to te backend of the server
 */

namespace HackatonWebApp.Backend
{
    public class jsonParser
    {
     public List<gridInfo> reveiveData(DateTime currentDateTime)
        {
            //simple check if the reuquest date is legit
            if (currentDateTime >= new DateTime(2012, 10, 15, 00, 00, 00) && currentDateTime <= new DateTime(2012, 10, 31, 00, 00, 00))
            {
                String modelForRequest = "";
                String startTime = "";
                String endTime = "";
                if (currentDateTime.Hour > 12) {
                    modelForRequest = currentDateTime.ToString("yyyyMMdd") + "120000";
                    DateTime startDay = currentDateTime.AddDays(1);
                    DateTime endDay = currentDateTime.AddDays(10);
                    startTime = startDay.ToString("yyyyMMdd") + "180000";
                    endTime = endDay.ToString("yyyyMMdd") + "120000";
                }
                else
                {
                    modelForRequest = currentDateTime.ToString("yyyyMMdd") + "000000";
                    DateTime startDay = currentDateTime.AddDays(1);
                    DateTime endDay = currentDateTime.AddDays(10);
                    startTime = startDay.ToString("yyyyMMdd") + "060000";
                    endTime = endDay.ToString("yyyyMMdd") + "000000";
                }

                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create("http://proxy.hydronet.com/api/Hn4Api/post/?postfix=ensemblegrids/get&proxytoken=");
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
     
                using (StreamWriter streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    //sorry for this ugly solution
                    String json = "{\"Readers\":[{\"DataSourceCode\":\"ECMWF.forecast.Sandy.NY\",\"Settings\":{\"VariableCodes\":[\"p\",\"VGRD:10m\"],\"EndDate\":\""+ endTime + "\",\"StartDate\":\"" + startTime + "\",\"ModelDate\":\"" + modelForRequest + "\",\"TimeZone\":\"UTC\",\"EnsembleNames\":[\"0\"],\"Extent\":{\"XLL\":-8396806.007097,\"YLL\": 4846055.280434,\"XUR\":-7988171.022234,\"YUR\":  5113012.701579,\"SpatialReference\":{\"EPSG\":3857}}}}]}";

                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                List<gridInfo> gridHolder = new List<gridInfo>();
                using (StreamReader reader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    js.MaxJsonLength = (int)httpResponse.ContentLength + 50;

                    var jsonObject = js.Deserialize<dynamic>(reader.ReadToEnd());
                    var partisipationObject = jsonObject["Data"][0];
                    var windObject = jsonObject["Data"][1];

                    bool firstRun = false;                    

                    foreach (var timeObject in partisipationObject["Ensembles"]["0"]["Grids"])
                    {
                        DateTime tempTime = DateTime.ParseExact(timeObject["StartDate"], "yyyyMMddHHmmss", CultureInfo.InvariantCulture);

                        int columnCounter = 1;
                        int gridCounter = 1;
                        int rowCounter = 1;

                        foreach (var dataObject in timeObject["Data"])
                        {                            
                            if (!firstRun)
                            {
                                gridInfo newGrid = new gridInfo();
                                newGrid.coorspondendingTimes = new List<DateTime>();
                                newGrid.Precipitation = new List<float>();
                                newGrid.wind = new List<float>();
                                newGrid.x = ((float) timeObject["GridDefinition"]["Xll"]) + (float) 0.25 * (columnCounter - (float) 1.0);
                                newGrid.y = ((float) timeObject["GridDefinition"]["Yll"]) + (float) 0.25 * (rowCounter  - (float) 1.0);
                                gridHolder.Add(newGrid);
                            }
                            gridHolder[gridCounter-1].coorspondendingTimes.Add(tempTime);
                            gridHolder[gridCounter - 1].Precipitation.Add((float) dataObject);

                            columnCounter++;
                            if (columnCounter > timeObject["GridDefinition"]["Columns"])
                            {
                                columnCounter = 1;
                                rowCounter++;
                            }
                            gridCounter++;
                        }
                        firstRun = true;
                    }
                    //////////////////////////////////////////////////
                    foreach (var timeObject in windObject["Ensembles"]["0"]["Grids"])
                    {
                        int gridCounter = 1;

                        foreach (var dataObject in timeObject["Data"])
                        {
                            gridHolder[gridCounter - 1].wind.Add((float)dataObject);
                            gridCounter++;
                        }
                        firstRun = true;
                    }
                }
                return gridHolder;
            }
            else return null; //wrong date in request 
        }
    }
}