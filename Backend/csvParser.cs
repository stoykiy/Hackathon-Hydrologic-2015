﻿using HackatonWebApp.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
namespace HackatonWebApp.Backend
{
    public class csvParser
    {
        public List<townInformation> parseCSV()
        {

            List<townInformation> towns = new List<townInformation>();

            string pathToCSV = HttpContext.Current.Server.MapPath("~/Content/ny_townsAltered.csv");
            using (StreamReader rd = new StreamReader(pathToCSV))
            {
                while (!rd.EndOfStream)
                {
                    townInformation newTown = new townInformation();

                    List<string> splits = rd.ReadLine().Split(';').ToList();
                    newTown.townName = splits[0];
                    newTown.population = Int32.Parse(splits[1]);

                    List<float> tempList = new List<float>();
                    foreach (String lotLan in splits[2].Split(new string[] { "0.0" }, StringSplitOptions.None))
                    {
                        if (lotLan != "")
                        {
                            List<string> latLonParsed = lotLan.Split(',').ToList();
                            newTown.x.Add((float) double.Parse(latLonParsed[0], CultureInfo.InvariantCulture));//add x
                            newTown.y.Add((float) double.Parse(latLonParsed[1], CultureInfo.InvariantCulture));//add y to town information                            
                        }
                    }
                    towns.Add(newTown);
                }
            }
            return towns;
        }
    }
}