﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HackatonWebApp.Models
{
    public class townInformation
    {
        public int population;
        public string townName;
        public List<float> x = new List<float>();
        public List<float> y = new List<float>();
        public short riskFactor;
        public short riskFactorRain;
        public short riskFacorWind;
    }
}